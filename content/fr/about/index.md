---
title: "Qui suis-je ?"
description: ""
images: [perrine4.jpg]
draft: false
menu: main
weight: 2
---

J'ai à cœur de transmettre les connaissances et outils issus des disciplines auxquelles je me suis formée:
 * **hypnose**,
 * **naturopathie**,
 * mais également des notions de **permaculture**, issue de la pratique
 * et surtout la dynamique de l'**auto-soin** (ou du self-help en anglais)

 L'objectif est que chacun et chacune puisse s'approprier sa santé et être autonome dans la recherche du bien-être. Nul n'est meilleur expert de sa santé que soi-même, il faut parfois seulement avoir les clés de compréhension et le goût de l'expérimentation.

 {{< figure src="./images/perrine4.jpg" alt="Photo de Perrine" class="center">}}

Ce sont ces penchants qui m'ont fait d'abord rendue ingénieure en informatique, spécialisée en données et apprentissage automatique.
Mais après 5 années de salariat dans différentes startups, un fois l'émerveillement de l'apprentissage de l'ordinateur passé, j'ai eu le loisirs de me rendre compte que les problématiques sociales, humaines et environnementales actuelles ne peuvent pas être adressées par plus d'innovation technique, d'intelligence artificielle.


J'ai donc décidé de rechercher la magie là où elle est vraiment : dans le vivant.
Je me suis engagée dans cette quête, comme beaucoup d'autres : trouver du sens, m'aligner avec mes convictions et mes rêves. Les graines avaient été semées dès ma jeunesse normande. Il me fallait seulement les arroser un peu, car travailler en bureau à Paris ne les avait pas ménagées. Ma grand-mère et ma mère m'avaient transmis leurs connaissances et intérêt pour les plantes, et les métiers de la santé m'intéressent depuis toute petite, mais ceux que je connaissais semblaient reposer sur des bases éloignées de mes convictions : trop mécaniste, s'intéressant aux symptômes plutôt qu'aux causes des problématiques. J'ai quitté mon travail pour suivre une formation d'hypnose. Puis j'ai quitté Paris pour faire du wwoofing en permaculture et plantes aromatiques et médicinales.


Mes rencontres et découvertes m'ont fait converger vers une formation en naturopathie. J'ai pu y redécouvrir des connaissances empiriques que j'appliquais de manière instinctives, tout en en comprenant les fondements scientifiques. L'hygiène alimentaire, physique et émotionnelle (notamment par l'hypnose), en bref, l'hygiène de vie, font partie intégrante d'une bonne santé.
Il ne suffit pas de remplacer les médicaments par des plantes !




## Formations

* 2017 - 2019 : Praticien en hypnose ericksonnienne -  [ARCHE Hypnose Paris](https://www.arche-hypnose.com/larche/)
* 2019 : Naturopathe - [EuroNature Toulouse](https://www.euronature.fr/annuaire?departement=35")
* 2020 - 2021 : Gyn'écologie Holistique - Rites de femmes

Je mets un point d’honneur à me former en continu pour compléter mes connaissances. Je continue donc ce parcours par des formations plus ponctuelles et remises à niveau.

## Valeurs

Le changement et le mouvement sont au fondement de la vie. Il est donc très important pour moi de vous accompagner à devenir autonome dans l'écoute de vous, dans la conscience de votre corps et de ses rouages.
