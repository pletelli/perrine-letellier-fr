---
title: "Retour au réseau social de terrain"
description: "Le bouche à oreille existe-t'il encore ?"
date: 2022-08-01T22:32:19+01:00
author: "Perrine Letellier"
images: []
draft: false
categories: ["entreprenariat", "informatique", "communication"]
---

Je n'aime pas tellement les réseaux sociaux, parce qu'ils sont construits sur la captation de l'attention et la monétisation de celle-ci (par la publicité). Aussi, ils tendent à uniformiser et normaliser le discours, à faire émerger toujours les mêmes profils. En attendant que mon site soit tout à fait prêt, je vais tenter de les utiliser on suivant mon but premier, partager un peu de mes connaissances, découvertes et expérimentations.
