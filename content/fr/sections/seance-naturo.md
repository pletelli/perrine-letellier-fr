---
title : "Séance de naturopathie"
description: ""
draft: false
weight: 1
---


Je vous reçois en consultation de naturopathie :
- générale et ouverte à toutes et tous / et pour tous les âges
- périnatale pour vous accompagner du désir d’enfant au post-partum
- péri-féminité : pour accompagner les femmes dans tous les changements de leur vie et leurs besoins spécifiques, leurs cycles / des premières règles à la ménopause

Ensemble, nous prenons le temps de faire un bilan complet de tout ce qui fait la santé, aussi bien physique, qu'émotionnelle, sociale et d'identifier les causes profondes de vos problématiques de santé.
Suite à la séance vous recevrez une fiche de conseils personnalisés pour améliorer la problématique avec laquelle vous serez venu⋅e. Mes outils sont les conseils en alimentation, en hygiène de vie, en plantes et huiles essentielles.

Le suivi permet de faire un point, d'ancrer les changements dans la durée, d'en mettre en place de nouveaux et d'avancer dans une perspective préventive.

Une première séance de naturopathie dure de **1h30** à **2h** et coûte **70€**.
Une séance de suivi dure maximum **1h30** et coûte **60€**.

Je ne prends pas la carte bancaire.
Aujourd’hui, un certain nombre de mutuelles prennent en charge en partie ou en totalité les frais de consultation en naturopathie. N’hésitez pas à appeler votre mutuelle pour vous renseigner.