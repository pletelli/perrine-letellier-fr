---
title : "Des ateliers collectifs"
description: ""
draft: false
weight: 4
---

Je souhaite proposer un nouveau cycle d'atelier d'auto-santé gynécologique pour les femmes et personnes à vulve et vagin. Le but est de faciliter leur réappropriation des connaissances anatomiques, physiologies, culturelles autour de la santé gynécologique et sexuelle et d'ouvrir un espace de parole et de partage.
Contactez-moi si vous souhaitez être tenu.e au courant.

Les ateliers collectifs passés que je peux proposer dans d'autres contextes :
 - cycle de 9 ateliers d'**auto-santé gynécologique** donnés mensuellement à Janzé en 2023
 - atelier sur **la santé de ma vulve**, octobre 2023 au forum Séisme Grand Ouest de Rennes
 - conférence sur **les solutions naturelles au douleurs vulvaires chroniques**, octobre 2023 à la Journée des douleurs du sexe féminin de Noisy Le Sec
 - atelier théorique et pratique sur les **légumes lacto-fermentés** et leurs bienfaits, mars 2023, *Le Pays fait son jardin*, au Theil de Bretagne 
 - atelier dédié au **bien-être ostéo-articulaire** avec confection de **baume de consoude**, novembre 2022, avec *Le jardin des merveilles* à Pléchâtel
 - atelier parents-enfants dédié à l'**alimentation saine**, *Le Pays fait son jardin*, au Theil de Bretagne 
 - atelier aromatologie, novembre 2021, à la piscine Les Ondines à Janzé

Je suis à votre disposition si vous souhaitez proposer un atelier sur ces thématiques dans votre structure/famille.