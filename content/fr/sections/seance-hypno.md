---
title : "Séance d'hypnose"
description: ""
draft: false
weight: 2
---




Je vous reçois en consultation d'hypnose :
- pour accompagner les émotions et les aider à circuler
- faire le pont entre le corps et les émotions
- soigner les traumatismes et les blessures de la vie et de son parcours
- redonner toutes les clefs pour être prêt au changement

Les champs d’application de l’hypnose sont nombreux. En voici une liste non-exhaustive :﻿ stress, anxiété, troubles du sommeil, addictions, deuils, séparations, préparation mentale (examen, confiance en soi), peurs ...

Une séance de naturopathie dure **1h30** et coûte **60€**.

Un tarif réduit est possible dans le cadre d'un suivi sur plusieurs techniques (naturopathie-hypnose-réflexologie) et séances.
