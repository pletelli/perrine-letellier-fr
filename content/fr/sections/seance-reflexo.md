---
title : "Séance de réflexologie"
description: ""
draft: false
weight: 3
---

La séance commence par un entretien qui permettra de personnaliser le soin. Le protocole est adapté à la problématique que vous aurez apportée. Bien entendu ce peut aussi être une séance de bien-être/détente.

Cette technique de pressions manuelles sur les pieds permet d'agir sur des fonctions organiques, de remédier à des déséquilibre.

Une séance de naturopathie dure **1h** et coûte **50€**.
