---
title: "Naturopathie"
description: "Qu'est-ce que c'est ? Qu'est-ce que ce n'est pas ?"
date: 2018-12-19T22:32:19+01:00
publishDate: 2018-12-19T22:21:42+01:00
author: "Perrine Letellier"
images: []
draft: false
---

La naturopathie est considérée par l'OMS comme médecine traditionnelle depuis 2001.

La naturopathie ne se substitue pas à la médecine, les deux approches sont complémentaires.
Elle est avant tout préventive et contribue à stimuler votre force vitale et le potentiel d'auto-régulation et d'auto-guérison de votre corps.

C'est une approche de la santé qui s'intéresse à l'individu dans sa globalité : corps physique, émotions, mental, voire spiritualité.
Toutes ces dimensions participent à la santé. On dit donc que c'est une discipline holistique.

La naturopathie s'intéresse à trouver la cause de la problématique de santé, et non pas seulement à en éviter les symptômes.
Elle cherche même à débusquer la cause, de la cause, de la cause.
L'hypothèse de base est que le symptôme est là comme un signal d'un déséquilibre.
C'est une approche causaliste.

En tant que naturopathe je suis avant tout éducatrice de santé.
L'idée est de vous transmettre les clés de compréhension de la physiologie, de la nutrition, etc, pour que vous puissiez vous même être autonome vers la santé !

Les conseils que je propose sont personnalisés et adaptés à votre personnalité mais aussi à votre vitalité.
Je peux être amenée à conseiller différentes techniques saines et naturelles :
* aliments : réglages alimentaires, conseils en nutritionnelles
* corps : conseils en pratique corporelle, réflexologie plantaire
* émotions et mental : écoute active, hypnose, gestion du stress
* air : respiration, exercices physiques
* eau : bains chauds, froids, sauna, hammam ...
* plantes : phytothérapie, fleurs de Bach, huiles essentielles

Les compléments alimentaires peuvent être nécessaires ponctuellement, mais l'idée est de rétablir un équilibre avant tout grâce à l'hygiène de vie ...

Et dans mes accompagnement j'ajoute ma touche :
* écoféministe
* inspiré du self-help/self-care
* un brin anarchiste
Mais je vous raconterais plus de moi dans un prochain post sans doute !

# Choix des outils
La naturopathie c'est un vrai travail de fond.
Afin d'établir un bilan de vitalité, je suis amenée à te poser de nombreuses questions pour mieux appréhender ton fonctionnement global, tes habitudes, ton histoire, etc.
Les questions concernent l'alimentation, la digestion, le sommeil, la gestion du stress, etc.

# La naturopathie se base sur 5 piliers : hygiènisme, vitalisme, causalisme, holisme, humorisme.

La naturopathie est *holistique*
Ne dissocie par corps de l'esprit

C'est à dire qu'on s'intéresse

La naturopathie est *causaliste*

Je m'intéresse avec toi à chercher la cause de ta problématique de santé, et non pas seulement à en éviter les symptômes.
Et encore mieux je m'intéresse à la cause de la cause de la cause de cette problématique.
L'hypothèse de travail sera que le symptôme est là pour t'indiquer un disfonctionnement plus profond.
L'amélioration sera durable

La naturopathie est *vitaliste*
Je m'intéresse avant tout à toi en tant que personne, la problèmatique de santé, ou maladie, n'est pas mon focus.

La naturopathie est *hygièniste*

La naturopathie est *humoriste*

La naturopathie propose une approche personnalisée, adaptée à votre personnalité mais aussi à votre vitalité

# À qui s'adresse la naturopathie ?
Il serait facile de faire une liste de problématique de santé qui peuvent vous inciter à prendre RDV, mais celle-ci serait longue, puisqu'en allant chercher leurs causes de nombreux symptômes pourront être améliorés.


# Associations ou fédérations pour mes pratiques
* OMNES (Organisation de la Médecine Naturelle et de l'Education Sanitaire)
* FENA (Fédération francaise des Ecoles de naturopathie)
