---
title: "Hypnose"
description: "The 6 best dish review twitter feeds to follow."
date: 2018-12-19T22:32:19+01:00
publishDate: 2018-12-19T22:21:42+01:00
author: "Perrine Letellier"
images: []
draft: false
---
L'hypnose ericksonienne.

Avant d'être naturopathe, je suis praticienne en hypnose ericksonienne.
J'ai été certifiée à l'automne 2018 d'une formation à l'ARCHE (Académie de Recherche en Hypnose Ericksonnienne) à l'automne 2017.
J'ai commencé cette formation juste après avoir démissionné d'un poste de Data Scientist-Data Engineer dans une startup parisienne avec la ferme volonté de ne plus habiter Paris, et de ne plus travailler pour des entreprises inutiles voire néfastes (marketing, publicité, banque, assurance, GAFAM) dans un contexte de crise écologique et sociale.

L'hypnose ericksonnienne c'est une hypnose

# Qu'est-ce que l'hypnose ?
La transe ou état de conscience modifié
On peut donc parler d'hypnoses au pluriel, car le panel de comportement est vaste.


# L'inconscient
L'inconscient n'est pas une réalité scientifique (un peu comme l'islamogauchisme en fait)

# L'accompagnement en hypnose
Bienveillant, non jugeant
La solution vient de vous et non de moi.
C'est le même principe qu'en naturopathie : on va chercher la cause de la cause de la cause.
Un des raisons souvent évoquée est l'arrêt du tabac, nous travaillerons alors sur les raisons qui vous poussent à vouloir arrêter, mais aussi elles qui vous poussent à fumer.
Ce n'est pas magique mais demande une implication personnelle.
Mon accompagnement est basé sur les connaissances scientifiques actuelles. Je mets un point d'honneur à me tenir au courant des dernières avancées.

# Les croyances limitantes
